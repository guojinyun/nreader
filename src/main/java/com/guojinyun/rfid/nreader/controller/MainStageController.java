package com.guojinyun.rfid.nreader.controller;

import Reader.ReaderAPI;
import com.guojinyun.rfid.nreader.NreaderApplication;
import com.guojinyun.rfid.nreader.model.Config;
import com.guojinyun.rfid.nreader.utils.ConfigUtil;
import com.guojinyun.rfid.nreader.view.SettingsDialogView;
import de.felixroske.jfxsupport.FXMLController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Modality;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author guojinyun
 * @date 19-11-12
 */
@FXMLController
public class MainStageController implements Initializable {

    private ResourceBundle resourceBundle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;
    }

    @FXML
    public void showSettingView(ActionEvent actionEvent) {
        NreaderApplication.showView(SettingsDialogView.class, Modality.WINDOW_MODAL);
    }

    @FXML
    public void handleConnectBtn(ActionEvent actionEvent) {
        int hScanner[] = new int[1];
        try {
            Config config = ConfigUtil.loadConfig("./config.ini");
            int res = ReaderAPI.Net_ConnectScanner(hScanner,
                    config.getReaderIP(),
                    config.getReaderPort(),
                    config.getHostIP(),
                    config.getReaderPort());
            if (res == ReaderAPI._OK) {
                System.out.println("connect successful");
                String title = NreaderApplication.getStage().getTitle()+" connect successful";
                NreaderApplication.getStage().setTitle(title);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
