package com.guojinyun.rfid.nreader.controller;

import com.guojinyun.rfid.nreader.NreaderApplication;
import com.guojinyun.rfid.nreader.model.Config;
import com.guojinyun.rfid.nreader.utils.ConfigUtil;
import com.guojinyun.rfid.nreader.view.SettingsDialogView;
import de.felixroske.jfxsupport.FXMLController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author guojinyun
 * @date 19-11-12
 */
@FXMLController
@Slf4j
public class SettingsDialogController implements Initializable {

    @FXML private ToggleGroup connectMode;
    @FXML private TextField readerIP;
    @FXML private TextField readerPort;
    @FXML private TextField hostIP;
    @FXML private TextField hostPort;
    @FXML private TextField rs232Port;
    @FXML private TextField databaseHost;
    @FXML private TextField databasePort;
    @FXML private TextField databaseName;
    @FXML private TextField databaseUsername;
    @FXML private TextField databasePassword;
    @FXML private TextField warningEmails;
    @FXML private TextField warningCount;
    @FXML private CheckBox ant1;
    @FXML private CheckBox ant2;
    @FXML private CheckBox ant3;
    @FXML private CheckBox ant4;
    @FXML private TextField readInterval;
    @FXML private Button loadBtn;
    @FXML private Button saveBtn;
    @FXML private Button cancelBtn;

    private ResourceBundle resourceBundle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;
        changeConnectMode(false);
    }

    @FXML
    public void handleLoadBtn(ActionEvent actionEvent) {
        try {
            Config config = ConfigUtil.loadConfig("./config.ini");
            connectMode.selectToggle(new ToggleButton(config.getConnectMode().getValue()));
            readerIP.setText(config.getReaderIP());
            readerPort.setText(config.getReaderPort()+"");
            hostIP.setText(config.getHostIP());
            hostPort.setText(config.getHostPort()+"");
            rs232Port.setText(config.getRs232Port());

            databaseHost.setText(config.getDatabaseHost());
            databasePort.setText(config.getDatabasePort());
            databaseName.setText(config.getDatabaseName());
            databaseUsername.setText(config.getDatabaseUsername());
            databasePassword.setText(config.getDatabasePassword());

            warningEmails.setText(config.getWarningEmails());
            warningCount.setText(config.getWarningCount());

            ant1.setSelected(config.getAnt1());
            ant2.setSelected(config.getAnt2());
            ant3.setSelected(config.getAnt3());
            ant4.setSelected(config.getAnt4());
            readInterval.setText(config.getReadInterval());

        } catch (IOException e) {
            log.info("指定的配置文件不存在");
            e.printStackTrace();
        }
    }

    @FXML
    public void handleSaveBtn(ActionEvent actionEvent) {
        Config config = new Config()
                .connectMode(connectMode.getSelectedToggle().getUserData().toString())
                .readerIP(readerIP.getText().trim())
                .readerPort(readerPort.getText().trim())
                .hostIP(hostIP.getText().trim())
                .hostPort(hostPort.getText().trim())
                .rs232Port(rs232Port.getText().trim())
                .databaseHost(databaseHost.getText().trim())
                .databasePort(databasePort.getText().trim())
                .databaseName(databaseName.getText().trim())
                .databaseUsername(databaseUsername.getText().trim())
                .databasePassword(databasePassword.getText().trim())
                .warningEmails(warningEmails.getText().trim())
                .databaseUsername(databaseUsername.getText().trim())
                .warningCount(warningCount.getText().trim())
                .readInterval(readInterval.getText().trim())
                .ant(ant1.isSelected(),ant2.isSelected(),ant3.isSelected(),ant4.isSelected());
        try {
            ConfigUtil.saveConfig("./config.ini",config);
        } catch (IOException e) {
            log.info("保存config.ini失败！");
            e.printStackTrace();
        }
        Stage stage = (Stage)saveBtn.getScene().getWindow();
        stage.close();
    }

    private void changeConnectMode(boolean isRs232){
        readerIP.setDisable(isRs232);
        readerPort.setDisable(isRs232);
        hostIP.setDisable(isRs232);
        hostPort.setDisable(isRs232);
        rs232Port.setDisable(!isRs232);
    }

    @FXML
    public void handleTcpIpMode(ActionEvent actionEvent) {
        changeConnectMode(false);
    }

    @FXML
    public void handleRs232Mode(ActionEvent actionEvent) {
        changeConnectMode(true);
    }

    @FXML
    public void handleCancelBtn(ActionEvent actionEvent) {
        System.out.println();
        System.out.println(resourceBundle.getString("settings.title"));
    }
}
