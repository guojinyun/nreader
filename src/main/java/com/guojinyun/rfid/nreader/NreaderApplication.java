package com.guojinyun.rfid.nreader;

import com.guojinyun.rfid.nreader.view.MainStageView;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import de.felixroske.jfxsupport.SplashScreen;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NreaderApplication extends AbstractJavaFxApplicationSupport {

    public static void main(String[] args) {
        launch(NreaderApplication.class, MainStageView.class, new SplashScreen(), args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        super.start(stage);
    }

}
