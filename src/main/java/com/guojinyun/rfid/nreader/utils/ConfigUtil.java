package com.guojinyun.rfid.nreader.utils;

import com.guojinyun.rfid.nreader.model.Config;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.ini4j.Ini;
import org.ini4j.Profile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author guojinyun
 * @date 19-11-13
 */
@Slf4j
public class ConfigUtil {


    public static boolean saveConfig(String filePath,Config config) throws IOException {
        File file = new File(filePath);
        if(file.exists()){
            FileUtils.deleteQuietly(file);
        }
        file.createNewFile();
        Ini ini = new Ini();
        ini.load(file);

        List<Config.ConfigItem> configList = config.getConfigList();
        configList.stream().forEach((item) -> {
            ini.add(item.getSection(),item.getKey(),StringUtils.defaultString(item.getValue(),""));
        });
        //将文件内容保存到文件中
        ini.store(file);

        return true;
    }

    public static Config loadConfig(String filePath) throws IOException {
        Config config =new Config();

        File file = new File(filePath);
        if(!file.exists()){
            return config;
        }
        Ini ini = new Ini();
        ini.load(file);

        Profile.Section basic = ini.get("basic");
        boolean ant1 = basic.get("ant1",Boolean.class);
        boolean ant2 = basic.get("ant2",Boolean.class);
        boolean ant3 = basic.get("ant3",Boolean.class);
        boolean ant4 = basic.get("ant4",Boolean.class);
        config.ant(ant1,ant2,ant3,ant4);
        String readInterval = basic.get("readInterval");
        config.readInterval(readInterval);
        Profile.Section warning = ini.get("warning");
        String warningEmails = warning.get("warningEmails");
        config.warningEmails(warningEmails);
        String warningCount = warning.get("warningCount");
        config.warningCount(warningCount);
        Profile.Section database = ini.get("database");
        String databaseHost = database.get("databaseHost");
        config.databaseHost(databaseHost);
        String databasePort = database.get("databasePort");
        config.databasePort(databasePort);
        String databaseName = database.get("databaseName");
        config.databaseName(databaseName);
        String databaseUsername = database.get("databaseUsername");
        config.databaseUsername(databaseUsername);
        String databasePassword = database.get("databasePassword");
        config.databasePassword(databasePassword);
        Profile.Section connect = ini.get("connect");
        String connectMode = connect.get("connectMode");
        config.connectMode(connectMode);
        String readerIP = connect.get("readerIP");
        config.readerIP(readerIP);
        String readerPort = connect.get("readerPort");
        config.readerPort(readerPort);
        String hostIP = connect.get("hostIP");
        config.hostIP(hostIP);
        String hostPort = connect.get("hostPort");
        config.hostPort(hostPort);
        String rs232Port = connect.get("rs232Port");
        config.rs232Port(rs232Port);
        return config;
    }


}
