package com.guojinyun.rfid.nreader.model;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @author guojinyun
 * @date 19-11-13
 */
@Data
public class Config {
    List<ConfigItem> itemList;
    private ConfigItem connectMode;
    private ConfigItem readerIP;
    private ConfigItem readerPort;
    private ConfigItem hostIP;
    private ConfigItem hostPort;
    private ConfigItem rs232Port;
    private ConfigItem databaseHost;
    private ConfigItem databasePort;
    private ConfigItem databaseName;
    private ConfigItem databaseUsername;
    private ConfigItem databasePassword;
    private ConfigItem warningEmails;
    private ConfigItem warningCount;
    private ConfigItem ant1;
    private ConfigItem ant2;
    private ConfigItem ant3;
    private ConfigItem ant4;
    private ConfigItem readInterval;

    public List<ConfigItem> getConfigList() {
        itemList = Lists.newArrayList(
                connectMode,
                readerIP,
                readerPort,
                hostIP,
                hostPort,
                rs232Port,
                databaseHost,
                databasePort,
                databaseName,
                databaseUsername,
                databasePassword,
                warningEmails,
                warningCount,
                ant1,
                ant2,
                ant3,
                ant4,
                readInterval);
        return itemList;
    }

    public Config connectMode(String connectMode){
        this.connectMode = new ConfigItem("connect","connectMode",connectMode);
        return this;
    }

    public Config readerIP(String readerIP) {
        this.readerIP = new ConfigItem("connect", "readerIP", readerIP);
        return this;
    }

    public Config readerPort(String readerPort) {
        this.readerPort = new ConfigItem("connect", "readerPort", readerPort);
        return this;
    }

    public Config hostIP(String hostIP) {
        this.hostIP = new ConfigItem("connect", "hostIP", hostIP);
        return this;
    }

    public Config hostPort(String hostPort) {
        this.hostPort = new ConfigItem("connect", "hostPort", hostPort);
        return this;
    }

    public Config rs232Port(String rs232Port) {
        this.rs232Port = new ConfigItem("connect", "rs232Port", rs232Port);
        return this;
    }

    public Config databaseHost(String databaseHost) {
        this.databaseHost = new ConfigItem("database", "databaseHost", databaseHost);
        return this;
    }

    public Config databasePort(String databasePort) {
        this.databasePort = new ConfigItem("database", "databasePort", databasePort);
        return this;
    }

    public Config databaseName(String databaseName) {
        this.databaseName = new ConfigItem("database", "databaseName", databaseName);
        return this;
    }

    public Config databaseUsername(String databaseUsername) {
        this.databaseUsername = new ConfigItem("database", "databaseUsername", databaseUsername);
        return this;
    }

    public Config databasePassword(String databasePassword) {
        this.databasePassword = new ConfigItem("database", "databasePassword", databasePassword);
        return this;
    }

    public Config warningEmails(String warningEmails) {
        this.warningEmails = new ConfigItem("warning", "warningEmails", warningEmails);
        return this;
    }

    public Config warningCount(String warningCount) {
        if (StringUtils.isNotBlank(warningCount)) {
            this.warningCount = new ConfigItem("warning", "warningCount", warningCount);
        } else {
            this.warningCount = new ConfigItem("warning", "warningCount", "500");
        }
        return this;
    }

    public Config ant(boolean ant1, boolean ant2, boolean ant3, boolean ant4) {
        this.ant1 = new ConfigItem("basic", "ant1", ant1 ? "true" : "false");
        this.ant2 = new ConfigItem("basic", "ant2", ant2 ? "true" : "false");
        this.ant3 = new ConfigItem("basic", "ant3", ant3 ? "true" : "false");
        this.ant4 = new ConfigItem("basic", "ant4", ant4 ? "true" : "false");
        return this;
    }

    public Config readInterval(String readInterval) {
        if (StringUtils.isNotBlank(readInterval)) {
            this.readInterval = new ConfigItem("basic", "readInterval", readInterval);
        } else {
            this.readInterval = new ConfigItem("basic", "readInterval", "500");
        }
        return this;
    }

    public ConfigItem getConnectMode() {
        return connectMode;
    }

    public String getReaderIP() {
        return readerIP.value;
    }

    public int getReaderPort() {
        return Integer.parseInt(readerPort.value);
    }

    public String getHostIP() {
        return hostIP.value;
    }

    public int getHostPort() {
        return Integer.parseInt(hostPort.value);
    }

    public String getRs232Port() {
        return rs232Port.value;
    }

    public String getDatabaseHost() {
        return databaseHost.value;
    }

    public String getDatabasePort() {
        return databasePort.value;
    }

    public String getDatabaseName() {
        return databaseName.value;
    }

    public String getDatabaseUsername() {
        return databaseUsername.value;
    }

    public String getDatabasePassword() {
        return databasePassword.value;
    }

    public String getWarningEmails() {
        return warningEmails.value;
    }

    public String getWarningCount() {
        return warningCount.value;
    }

    public boolean getAnt1() {
        return "true".equals(ant1.getValue());
    }

    public boolean getAnt2() {
        return "true".equals(ant2.getValue());
    }

    public boolean getAnt3() {
        return "true".equals(ant3.getValue());
    }

    public boolean getAnt4() {
        return "true".equals(ant4.getValue());
    }

    public String getReadInterval() {
        return readInterval.value;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class ConfigItem {
        private String section;
        private String key;
        private String value;
    }

}
