package com.guojinyun.rfid.nreader.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @author guojinyun
 * @date 19-11-12
 */
@FXMLView(value = "/fxml/SettingsDialogView.fxml",title = "settings",bundle = "i18n",encoding = "utf-8")
public class SettingsDialogView extends AbstractFxmlView {
}
