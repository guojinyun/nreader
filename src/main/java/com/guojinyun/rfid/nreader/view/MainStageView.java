package com.guojinyun.rfid.nreader.view;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @author guojinyun
 * @date 19-11-12
 */
@FXMLView(value = "/fxml/MainStage.fxml",title = "index",bundle = "i18n.index",encoding = "utf-8")
public class MainStageView extends AbstractFxmlView {
}
